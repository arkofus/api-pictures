/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'
import * as fs from "fs";
import {randomUUID} from "crypto";

interface PictureType {
  id: string, latitude: number, longitude: number, file: string
}

const picturesSaved = <PictureType[]>[];

Route.get('/', async ({ view }) => {
  return view.render('welcome')
})

Route.post('/',  ({ request, response }) => {
  const all = request.body();
  console.log("post picture");

  //Image en URL not work
  if(!all.picture) {
     return response.status(404);
  }

  console.log("check dir pictures")
  if(!fs.existsSync("build/public/pictures")) {
    console.log("create dir");
    fs.mkdirSync("build/public/pictures");
  }

  const latitude = all.latitude;
  const longitude = all.longitude;

  const base64 = all.picture;
  const id = randomUUID();
  const fileName = id+".jpg";
  try {
    console.log("create file");
    fs.writeFileSync("build/public/pictures/"+fileName, base64, "base64");
  } catch (e) {
    console.log(e);
    return response.status(404);
  }

  const image: PictureType = {id, latitude, longitude, file:fileName};

  picturesSaved.push(image);

  return response.status(200).json(image);
});

Route.get('/pictures', () => {

  console.log("get pictures")

  if(!fs.existsSync("build/public/pictures")) {
    console.log("create dir pictures")
    fs.mkdirSync("build/public/pictures");
    return [];
  }

  return fs.readdirSync("build/public/pictures");
})

Route.get('/infos', () => {
  return picturesSaved;
})

Route.get('/infos/:id', ({ request })=> {
  const id = request.params().id;

  picturesSaved.forEach((p) => {
    if(p.id == id) {
      return p;
    }
  })

  return { id: "-1", latitude: 0, longitude: 0, file: ""};
});

